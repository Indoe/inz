package com.piotr.pawlowski.application.controllers;

import com.piotr.pawlowski.application.models.Reservation;
import com.piotr.pawlowski.application.models.Room;
import com.piotr.pawlowski.application.models.User;
import com.piotr.pawlowski.application.repositories.ReservationRepository;
import com.piotr.pawlowski.application.repositories.RoomRepository;
import com.piotr.pawlowski.application.repositories.SubjectRepository;
import com.piotr.pawlowski.application.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/reservation")
@EnableScheduling
@CrossOrigin(origins = "http://localhost:4200")
public class ReservationController {

    @Autowired
    ReservationRepository reservationRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SubjectRepository subjectRepository;


    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<Reservation> getAllReservations() {
        List<Reservation> reservations = reservationRepository.findAll();
        return reservations;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/create/{cycle}/{username}")
    public Boolean createReservation(@RequestBody Reservation reservation, @PathVariable Integer cycle, @PathVariable String username) {
        try {
            if (!reservation.getStartDate().before(reservation.getEndDate())) {
                return false;
            }
            User user = userRepository.findByUsername(username);
            reservation.setUser(user);
            Date startDate = reservation.getStartDate();
            Date endDate = reservation.getEndDate();
            Calendar calendarStart = Calendar.getInstance();
            Calendar calendarEnd = Calendar.getInstance();
            calendarStart.setTime(startDate);
            calendarEnd.setTime(endDate);
            for (int i = 0; i < cycle; i++) {
                List<Reservation> reservations = reservationRepository.getCheckReservation(reservation.getStartDate(), reservation.getEndDate(), reservation.getRoom());
                if (reservations.size() == 0) {
                    reservationRepository.save(reservation);
                }
                calendarStart.add(Calendar.DAY_OF_YEAR, 7);
                calendarEnd.add(Calendar.DAY_OF_YEAR, 7);
                startDate = calendarStart.getTime();
                endDate = calendarStart.getTime();
                Reservation reservationTemp = new Reservation();
                reservationTemp.setEndDate(endDate);
                reservationTemp.setStartDate(startDate);
                reservationTemp.setUser(reservation.getUser());
                reservationTemp.setSubject(reservation.getSubject());
                reservationTemp.setRoom(reservation.getRoom());
                reservationTemp.setDescription(reservation.getDescription());
                reservation = reservationTemp;
            }
            sendSimpleMessage(reservation.getUser().getEmail(), "Rezerwacja: " + reservation.getDescription(),
                    "Rezerwacja użytkownika: " + reservation.getUser().getName() + " " + reservation.getUser().getSurname()
                            + "\n"
                            + "\n" + "W czasie"
                            + "\n" + "od:     " + reservation.getStartDate()
                            + "\n" + "do:     " + reservation.getEndDate()
                            + "\n"
                            + "\n" + "Pokój: " + reservation.getRoom().getName()
                            + "\n"
                            + "\n" + "Przedmiot: " + reservation.getSubject().getName());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/")
    public Boolean updateReservation(@RequestBody Reservation reservation) {
        try {

            if (!reservation.getStartDate().before(reservation.getEndDate())) {
                return false;
            }
            reservationRepository.save(reservation);
            sendSimpleMessage(reservation.getUser().getEmail(), "Rezerwacja: " + reservation.getDescription(),
                    "Rezerwacja użytkownika: " + reservation.getUser().getName() + " " + reservation.getUser().getSurname()
                            + "\n"
                            + "\n" + "W czasie"
                            + "\n" + "od:     " + reservation.getStartDate()
                            + "\n" + "do:     " + reservation.getEndDate()
                            + "\n"
                            + "\n" + "Pokój: " + reservation.getRoom().getName()
                            + "\n"
                            + "\n" + "Przedmiot: " + reservation.getSubject().getName());

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    @RequestMapping(method = RequestMethod.POST, value = "/delete/")
    public Boolean deleteReservation(@RequestBody Reservation reservation) {
        try {
            reservationRepository.delete(reservation);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @RequestMapping(method = RequestMethod.GET, value = "/getForWeek/{startTimeStamp}/{endTimeStamp}/{roomId}")
    public List<Reservation> getReservationsForWeek(@PathVariable String startTimeStamp, @PathVariable String endTimeStamp, @PathVariable Integer roomId) {
        Room room = roomRepository.findOne(roomId);
        Date start = convertUnixTimeStampToDate(startTimeStamp);
        Date end = convertUnixTimeStampToDate(endTimeStamp);
        System.out.println(start);
        System.out.println(end);

        List<Reservation> reservations = reservationRepository.getReservationByWeek(start, end, room);
        return reservations;
    }

    public Date convertUnixTimeStampToDate(String stringTimeStamp) {
        LocalDate localDate = Instant.ofEpochMilli(Long.parseLong(stringTimeStamp)).atZone(ZoneId.systemDefault()).toLocalDate();
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return date;
    }

    // W tej metodzie masz zrobic info o nadchodzacych wydarzeniach, tzn sciagnij wszystkie wydażenia na dzisiejszy dziem
    //i daj je w tresci maila
    //
    //
    // metoda Ktora SprawdzaC o Pół Godziny Czy W Liście Obiektów Obserwacji Sal nie znajduje sie taki obiekt dla ktorego zwolnil sie termin
    //    //po prostu zapusc funkcje ktora bedzie wyszukiwac rezerwacje w danych godzinach, jak dla jakiejsc obserwacji nic nie znajdzie to
    //    //to wtedy znaczy ze jest wolne i wyslij maila z info i tyle

    @Scheduled(cron = "0 0 7 * * ?")
    public void executeTasksAtSeven() {
        System.out.println("Sending email");

    }

    @Autowired
    public JavaMailSender emailSender;

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/change/{username}")
    public Boolean requestChangeReservation(@RequestBody Reservation reservation, @PathVariable String username) {
        try {
            sendSimpleMessage(reservation.getUser().getEmail(), "Prośba o zmianę rezerwacji: " + reservation.getDescription(),
                    "Prośba od użytkownika użytkownika: " + username + " " +
                            "Rezerwacja użytkownika: " + reservation.getUser().getName() + " " + reservation.getUser().getSurname()
                            + "\n"
                            + "\n" + "W czasie"
                            + "\n" + "od:     " + reservation.getStartDate()
                            + "\n" + "do:     " + reservation.getEndDate()
                            + "\n"
                            + "\n" + "Pokój: " + reservation.getRoom().getName()
                            + "\n"
                            + "\n" + "Przedmiot: " + reservation.getSubject().getName());
            return true;
        } catch (Exception e) {
            return false;
        }

    }


}
