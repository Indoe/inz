package com.piotr.pawlowski.application.repositories;

import com.piotr.pawlowski.application.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
//
//    List<User> findUserByName(String name);
//    List<User> findUserBySurname(String surname);
//    List<User> findUserByEmail(String email);
//    List<User> findUserByIsTeacher(Boolean isTeacher);
//    List<User> findUserByIsAdmin(Boolean isAdmin);
}
