package com.piotr.pawlowski.application.models;

import javax.persistence.*;

@Entity
public class RoomSwap {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "reservation_proposing_id", nullable = false)
    private Reservation reservationProposing;
    @ManyToOne
    @JoinColumn(name = "reservation_wanting_id", nullable = false)
    private Reservation reservationWanting;

    public RoomSwap() {

    }

    public RoomSwap(Reservation reservationProposing, Reservation reservationWanting) {
        this.reservationProposing = reservationProposing;
        this.reservationWanting = reservationWanting;
    }

    public Integer getId() {
        return id;
    }

    public Reservation getReservationProposing() {
        return reservationProposing;
    }

    public void setReservationProposing(Reservation reservationProposing) {
        this.reservationProposing = reservationProposing;
    }

    public Reservation getReservationWanting() {
        return reservationWanting;
    }

    public void setReservationWanting(Reservation reservationWanting) {
        this.reservationWanting = reservationWanting;
    }

    @Override
    public String toString() {
        return "RoomSwap{" +
                "id=" + id +
                ", reservationProposing=" + reservationProposing +
                ", reservationWanting=" + reservationWanting +
                '}';
    }
}
