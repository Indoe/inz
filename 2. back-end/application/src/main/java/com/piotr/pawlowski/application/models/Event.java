package com.piotr.pawlowski.application.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    @Column
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date date;
    @Column
    private String header;
    @Column
    private String description;

    public Event() {

    }

    public Event(User user, Date date, String header, String description) {
        this.user = user;
        this.date = date;
        this.header = header;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", user=" + user +
                ", date=" + date +
                ", header='" + header + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
