package com.piotr.pawlowski.application.controllers;

import com.piotr.pawlowski.application.models.Event;
import com.piotr.pawlowski.application.models.User;
import com.piotr.pawlowski.application.repositories.EventRepository;
import com.piotr.pawlowski.application.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/event")
@EnableScheduling
@CrossOrigin(origins = "http://localhost:4200")
public class EventController {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    UserRepository userRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/create/{username}")
    public Boolean createEvent(@RequestBody Event event, @PathVariable String username) {
        try {
            User user = userRepository.findByUsername(username);
            event.setUser(user);
            eventRepository.save(event);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<Event> getAllEvents() {
        List<Event> events = eventRepository.findAll();
        return events;
    }


    @RequestMapping(method = RequestMethod.POST, value = "/delete/")
    public Boolean deleteReservation(@RequestBody Event event) {
        try {
            eventRepository.delete(event);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/")
    public Boolean updateReservation(@RequestBody Event event) {
        try {
            eventRepository.save(event);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Autowired
    public JavaMailSender emailSender;

    private void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setText(text);
        message.setSubject(subject);
        emailSender.send(message);
    }


    @Scheduled(cron = "0 0 7 * * ?")
    public void sendInfoAboutTodayEvents() {
        Date startDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date endDate = calendar.getTime();
        List<Event> events = eventRepository.getEventsForToday(startDate, endDate);
        List<User> users = userRepository.findAll();
        if (events.size() > 0) {
            StringBuilder content = new StringBuilder("Wydarzenia: \n");
            for (Event event : events) {
                content.append(event.getHeader()).append(" ").append(event.getDescription()).append("\n");
            }
            for (User user : users) {
                sendSimpleMessage(user.getEmail(), "Wydarzenia", "Wydarzenia na dzień dzisiejszy: \n"
                        + content.toString());
            }
        }
    }
}
