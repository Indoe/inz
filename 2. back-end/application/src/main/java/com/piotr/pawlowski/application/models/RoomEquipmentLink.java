package com.piotr.pawlowski.application.models;

import javax.persistence.*;

@Entity
public class RoomEquipmentLink {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "equipment_id", nullable = false)
    private Equipment equipment;
    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false)
    private Room room;
    @Column
    private Integer quantity;

    public RoomEquipmentLink() {

    }

    public RoomEquipmentLink(Equipment equipment, Room room, Integer quantity) {
        this.equipment = equipment;
        this.room = room;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "RoomEquipmentLink{" +
                "id=" + id +
                ", equipment=" + equipment +
                ", room=" + room +
                ", quantity=" + quantity +
                '}';
    }
}
