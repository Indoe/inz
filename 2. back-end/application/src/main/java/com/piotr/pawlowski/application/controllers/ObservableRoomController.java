package com.piotr.pawlowski.application.controllers;


import com.piotr.pawlowski.application.models.ObservableRoom;
import com.piotr.pawlowski.application.models.Reservation;
import com.piotr.pawlowski.application.models.User;
import com.piotr.pawlowski.application.repositories.ObservableRoomRepository;
import com.piotr.pawlowski.application.repositories.ReservationRepository;
import com.piotr.pawlowski.application.repositories.RoomRepository;
import com.piotr.pawlowski.application.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/observableRoom")
@EnableScheduling
@CrossOrigin(origins = "http://localhost:4200")
public class ObservableRoomController {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private ObservableRoomRepository observableRoomRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/create/{username}")
    public Boolean createObservableRoom(@RequestBody ObservableRoom observableRoom, @PathVariable String username) {
        try {

            if (!observableRoom.getStartDate().before(observableRoom.getEndDate())) {
                return false;
            }
            User user = userRepository.findByUsername(username);
            observableRoom.setUser(user);
            System.out.println(user);
            observableRoomRepository.save(observableRoom);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/")
    public Boolean updateObservableRoom(@RequestBody ObservableRoom observableRoom) {
        try {

            if (!observableRoom.getStartDate().before(observableRoom.getEndDate())) {
                return false;
            }

            observableRoomRepository.save(observableRoom);

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Scheduled(cron = "0 */2 * ? * *")
    public void executeTasksEveryHalfHour() {
        System.out.println("Sprawdzam wykonanie funkcji " + new Date());
        List<ObservableRoom> observableRooms = observableRoomRepository.findAll();
        for (ObservableRoom observableRoom : observableRooms) {
            List<Reservation> reservations = reservationRepository.getCheckReservation(observableRoom.getStartDate(), observableRoom.getEndDate(), observableRoom.getRoom());
            if (reservations.size() == 0) {
                System.out.println("Have found vacant reservation");
                sendSimpleMessage(observableRoom.getUser().getEmail(), "Zwolniona rezerwacja: " + observableRoom.getRoom().getName(),
                        "Rezerwacja na pokój: " + observableRoom.getRoom().getName()
                                + "\n"
                                + "\n" + "W czasie"
                                + "\n" + "od:     " + observableRoom.getStartDate()
                                + "\n" + "do:     " + observableRoom.getEndDate()
                                + "\n" + "jest już dostępna");
                observableRoomRepository.delete(observableRoom);
            }
        }
    }


    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<ObservableRoom> getAllObservableRooms() {
        List<ObservableRoom> observableRooms = observableRoomRepository.findAll();
        return observableRooms;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete/")
    public Boolean deleteObservableRoom(@RequestBody ObservableRoom observableRoom) {
        try {
            observableRoomRepository.delete(observableRoom);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Autowired
    public JavaMailSender emailSender;

    public void sendSimpleMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setText(text);
        message.setSubject(subject);

        emailSender.send(message);
    }

}
