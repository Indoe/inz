package com.piotr.pawlowski.application.repositories;

import com.piotr.pawlowski.application.models.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectRepository extends JpaRepository<Subject, Integer> {
}
