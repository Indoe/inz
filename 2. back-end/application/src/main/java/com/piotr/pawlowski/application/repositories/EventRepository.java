package com.piotr.pawlowski.application.repositories;

import com.piotr.pawlowski.application.models.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface EventRepository extends JpaRepository<Event, Integer> {

    @Query("select e from Event e where e.date between :start and :end")
    List<Event> getEventsForToday(@Param("start") Date start, @Param("end") Date end);
}
