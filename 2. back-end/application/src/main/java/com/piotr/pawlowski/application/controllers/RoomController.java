package com.piotr.pawlowski.application.controllers;

import com.piotr.pawlowski.application.models.Room;
import com.piotr.pawlowski.application.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/room")
@CrossOrigin(origins = "http://localhost:4200")
public class RoomController {

    @Autowired
    RoomRepository roomRepository;

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<Room> getAllRooms() {
        List<Room> rooms = roomRepository.findAll();
        return rooms;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/")
    public Boolean updateRoom(@RequestBody Room room) {
        try {
            roomRepository.save(room);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete/")
    public Boolean deleteRoom(@RequestBody Room room) {
        try {
            roomRepository.delete(room);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
