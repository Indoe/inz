package com.piotr.pawlowski.application.repositories;

import com.piotr.pawlowski.application.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Integer> {

}