package com.piotr.pawlowski.application.controllers;

import com.piotr.pawlowski.application.models.Subject;
import com.piotr.pawlowski.application.repositories.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subject")
@CrossOrigin(origins = "http://localhost:4200")
public class SubjectController {

    @Autowired
    private SubjectRepository subjectRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/update/")
    public Boolean updateSubject(@RequestBody Subject subject) {
        try {
            subjectRepository.save(subject);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<Subject> getAllSubject() {
        List<Subject> subjects = subjectRepository.findAll();
        return subjects;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete/")
    public Boolean deleteSubject(@RequestBody Subject subject) {
        try {
            subjectRepository.delete(subject);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
