package com.piotr.pawlowski.application.repositories;

import com.piotr.pawlowski.application.models.ObservableRoom;
import com.piotr.pawlowski.application.models.Reservation;
import com.piotr.pawlowski.application.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ObservableRoomRepository extends JpaRepository<ObservableRoom, Integer> {
    @Query("select o from ObservableRoom o where o.room=:room and ((o.startDate >= :start) and (o.endDate <= :end))")
    List<Reservation> getCheckObservableRoom(@Param("start") Date start, @Param("end") Date end, @Param("room") Room room);
}
