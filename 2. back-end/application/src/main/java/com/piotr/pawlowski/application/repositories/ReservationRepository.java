package com.piotr.pawlowski.application.repositories;

import com.piotr.pawlowski.application.models.Reservation;
import com.piotr.pawlowski.application.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    @Query("select r from Reservation r where r.room=:room and (r.startDate between :start and :end)")
    List<Reservation> getReservationByWeek(@Param("start") Date start, @Param("end") Date end, @Param("room") Room room);

    @Query("select r from Reservation r where r.room=:room and ((r.startDate >= :start) and (r.endDate <= :end))")
    List<Reservation> getCheckReservation(@Param("start") Date start, @Param("end") Date end, @Param("room") Room room);


}


