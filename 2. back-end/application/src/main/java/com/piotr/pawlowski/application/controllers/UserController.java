package com.piotr.pawlowski.application.controllers;


import com.piotr.pawlowski.application.models.User;
import com.piotr.pawlowski.application.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

//    @RequestMapping(method = RequestMethod.POST, value = "/make/")
//    public Boolean createUser(@RequestBody User user) {
//        try {
//            user.setAdmin(false);
//            user.setTeacher(false);
//            userRepository.save(user);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }

    @RequestMapping(method = RequestMethod.POST, value = "/update/")
    public Boolean updateUser(@RequestBody User user) {
        try {
            userRepository.save(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<User> getAllUsers() {
        List<User> users = userRepository.findAll();
        return users;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/delete/")
    public Boolean deleteUser(@RequestBody User user) {
        try {
            userRepository.delete(user);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/get/{username}")
    public User findUserByName(@PathVariable String username) {
        User user = userRepository.findByUsername(username);
        return user;
    }

    //
//    @RequestMapping(method = RequestMethod.GET, value = "/surname/{surname}")
//    public List<User> findUserBySurname(@PathVariable String surname) {
//        List<User> users = userRepository.findUserBySurname(surname);
//        return users;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/email/{emial}")
//    public List<User> findUserByEmail(@PathVariable String email) {
//        List<User> users = userRepository.findUserByEmail(email);
//        return users;
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/isTeacher/{isTeacher}")
//    public List<User> findUserByIsTeacher(@PathVariable Boolean isTeacher) {
//        List<User> users = userRepository.findUserByIsTeacher(isTeacher);
//        return users;
//    }
//
    @RequestMapping(method = RequestMethod.GET, value = "/isAdministrator/{username}")
    public Boolean isUserAdministrator(@PathVariable String username) {
        User user = userRepository.findByUsername(username);
        return user.getAdmin();
    }


}
