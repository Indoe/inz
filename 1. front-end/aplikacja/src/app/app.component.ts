import { Component } from '@angular/core';
import { UserService } from './service/user.service';
import { AuthService } from './service/auth.service';
import { Router } from '@angular/router';
import { LoginService } from './service/login.service';
import { User } from './model/User';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  display = true;
  admin = false;

  constructor(
    private loginService: LoginService,
    private router: Router,
    private userService: UserService
  ) {
    this.getUser();
    userService.getUserSteram().subscribe((res: User) => {
      this.admin = res.admin;
    });
    this.userService.getLoggedUser(this.loginService.getCurrentUsername());
  }

  isUserLogged() {
    return this.loginService.isUserLogged();
  }

  hideIcons() {
    this.display = !this.display;
  }

  logout() {
    this.router.navigate(['login']);
    this.loginService.logout();
    this.admin = false;
  }

  getCurrentUsername() {
    return this.loginService.getCurrentUsername();
  }

  getUser() {
    const username = this.loginService.getCurrentUsername();
    this.userService.getUserSteram().subscribe((res: User) => {
      this.admin = res.admin;
    });
  }
}
