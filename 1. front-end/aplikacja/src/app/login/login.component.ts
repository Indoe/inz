import { Component, OnInit } from '@angular/core';
// tslint:disable-next-line:import-blacklist
import { Subscription } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../service/user.service';
import { AuthenticationData } from '../model/AuthenticationData';
import { LoginService } from '../service/login.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  returnUrl: any;
  loading: boolean;

  constructor(
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {}

  authenticate(authenticationData: AuthenticationData) {
    this.loginService.login(authenticationData).subscribe(
      res => {
        this.router.navigate(['/main']);
      },
      error => {
        alert('Wrong username or password, try again');
      }
    );
  }
}
