import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { ObservableRoom } from '../model/ObservableRoom';
import { UserService } from '../service/user.service';
import { ObservableRoomService } from '../service/observableRoom.service';
import { Room } from '../model/Room';
import { RoomService } from '../service/room.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'observable-room',
  templateUrl: './observable-room.component.html',
  styleUrls: ['./observable-room.component.css']
})
export class ObservableRoomComponent implements OnInit {
  observableRooms: ObservableRoom[];
  observableRoom: ObservableRoom;
  selected: ObservableRoom = null;
  room: Room;
  rooms: Room[];
  user: User;
  users: User[];
  mode = '';
  edited = {};

  edit(observableRoom) {
    this.mode = 'edit';
    this.edited = observableRoom;
    this.selected = observableRoom;
  }

  createNew(observableRoom) {
    this.mode = 'edit';
    this.selected = observableRoom;
    this.edited = observableRoom;
  }

  select(observableRoom) {
    if (observableRoom !== this.selected) {
      this.mode = 'selected';
    }
    this.selected = observableRoom;
  }

  constructor( private observableRoomService: ObservableRoomService,
    private userService: UserService,
    private roomService: RoomService ) {
    this.observableRoom = new ObservableRoom();

    this.observableRoomService
    .getAllObservableRooms()
    .subscribe((res: ObservableRoom[]) => {
      this.observableRooms = res;
    });

    this.userService.getAllUsers().subscribe((res: User[]) => {
      this.users = res;
    });

    this.roomService.getAllRooms().subscribe((res: Room[]) => {
      this.rooms = res;
    });
  }


  getAllObservableRooms() {
    this.observableRoomService
      .getAllObservableRooms()
      .subscribe((res: ObservableRoom[]) => {
        this.observableRooms = res;
        console.log(this.observableRooms);
      });
  }

  updateObservableRoom() {
    this.observableRoomService
      .updateObservableRoom(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('ObservableRoom added sucessfully');
          this.getAllObservableRooms();
          this.mode = '';
        } else {
          alert('ObservableRoom could not be add');
        }
      });
  }

  createObservableRoom() {
    this.observableRoomService
      .createObservableRoom(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('ObservableRoom added sucessfully');
          this.getAllObservableRooms();
          this.mode = '';
        } else {
          alert('ObservableRoom could not be add');
        }
      });
  }

  deleteObservableRoom() {
    console.log(this.observableRoom);
    this.observableRoomService
      .deleteObservableRoom(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('ObservableRoom delted sucessfully');
          this.mode = '';
          this.getAllObservableRooms();
        } else {
          alert('ObservableRoom could not be delete');
        }
      });
  }

  removeGMT(date1: Date) {
    const date = new Date(date1);
    // tslint:disable-next-line:no-construct
    // tslint:disable-next-line:quotemark
    // tslint:disable-next-line:no-construct
    let str = new String(date.toUTCString());
    str = str.slice(5, -7);
    return str;
  }

  ngOnInit() {}
}
