import { User } from './User';
import { Room } from './Room';
import { Data } from '@angular/router/src/config';
import { Subject } from './Subject';


export class Reservation {
  id: number;
  user: User;
  room: Room;
  subject: Subject;
  startDate: DateTimeFormat;
  endDate: DateTimeFormat;
  description: string;
}
