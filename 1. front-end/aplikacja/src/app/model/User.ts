export class User {
  id: number;
  name: string;
  surname: string;
  email: string;
  password: string;
  teacher: boolean;
  admin: boolean;
  username: string;
}
