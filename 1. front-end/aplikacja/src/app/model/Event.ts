import { User } from './User';

export class Happening {
  id: number;
  user: User;
  date: DateTimeFormat;
  header: string;
  description: string;
}
