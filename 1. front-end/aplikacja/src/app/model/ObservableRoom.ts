import { Room } from './Room';
import { User } from './User';
import { Data } from '@angular/router/src/config';

export class ObservableRoom {
  id: number;
  user: User;
  room: Room;
  startDate: DateTimeFormat;
  endDate: DateTimeFormat;
}
