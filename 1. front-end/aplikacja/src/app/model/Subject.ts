import { User } from './User';

export class Subject {
  id: number;
  name: string;
  teacher: User;
}
