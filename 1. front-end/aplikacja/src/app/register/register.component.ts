import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { RegisterService } from '../service/register.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  repeatedPassword: string;

  constructor(private registerService: RegisterService) {}

  ngOnInit() {}

  register(user: User) {
    if (this.isFormDataCorrect(user)) {
      this.registerService.register(user);
    }
  }

  isFormDataCorrect(user: User): boolean {
    if (
      this.areAllRequiredFieldsFilled(user) &&
      this.arePasswordsSame(user.password, this.repeatedPassword) &&
      this.doPaswordContainAllNeededCharacters(user.password)
    ) {
      return true;
    }
    return false;
  }

  areAllRequiredFieldsFilled(user: User): boolean {
    if (
      user.email &&
      user.name &&
      user.username &&
      user.password &&
      user.surname
    ) {
      return true;
    } else {
      window.alert('All requiired Fields must be filled');
      return false;
    }
  }

  arePasswordsSame(password: string, repeatedPassword: string): boolean {
    if (password !== repeatedPassword) {
      window.alert('Passwords needs to be the same');
      return false;
    }
    return true;
  }

  doPaswordContainAllNeededCharacters(password: String): boolean {
    const p = password;
    if (p.length < 8) {
      alert('Your password must be at least 8 characters');
      return false;
    }
    if (p.search(/[a-z]/i) < 0) {
      alert('Your password must contain at least one letter.');
      return false;
    }
    if (p.search(/[0-9]/) < 0) {
      alert('Your password must contain at least one digit.');
      return false;
    }
    return true;
  }
}
