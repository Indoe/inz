import { Component, OnInit } from '@angular/core';
import { RoomService } from '../service/room.service';
import { Room } from '../model/Room';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'all-rooms',
  templateUrl: './all-rooms.component.html',
  styleUrls: ['./all-rooms.component.css']
})
export class AllRoomsComponent implements OnInit {
  rooms: Room[];
  room: Room;
  selected: Room = null;
  mode = '';
  edited = {};

  edit(room) {
    this.mode = 'edit';
    this.edited = room;
    this.selected = room;
  }

  createNew(room) {
    this.mode = 'edit';
    this.selected = room;
    this.edited = room;
  }

  select(room) {
    if (room !== this.selected) {
      this.mode = 'selected';
    }
    this.selected = room;
  }

  constructor(private roomService: RoomService) {
    this.getAllRooms();
    this.room = new Room();
  }

  getAllRooms() {
    this.roomService.getAllRooms().subscribe((res: Room[]) => {
      this.rooms = res;
      console.log(this.rooms);
    });
  }

  updateRoom() {
    this.roomService.updateRoom(this.selected).subscribe((res: Boolean) => {
      console.log(res);
      if (res) {
        alert('Room added sucessfully');
        this.getAllRooms();
        this.mode = '';
      } else {
        alert('Room could not be add');
      }
    });
  }

  deleteRoom() {
    console.log(this.room);
    this.roomService.deleteRoom(this.selected).subscribe((res: Boolean) => {
      console.log(res);
      if (res) {
        alert('Room delted sucessfully');
        this.mode = '';
        this.getAllRooms();
      } else {
        alert('Room could not be delete');
      }
    });
  }

  ngOnInit() {}
}
