import { Component, OnInit } from '@angular/core';
import { EventService } from '../service/events.service';
import { Happening } from '../model/Event';
import * as moment from 'moment';
import { LoginService } from '../service/login.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'all-events',
  templateUrl: './all-events.component.html',
  styleUrls: ['./all-events.component.css']
})
export class AllEventsComponent implements OnInit {
  happenings: Happening[];

  selected: Happening = null;
  mode = '';
  edited = {};
  admin = false;
  display: Boolean;

  constructor(
    private eventService: EventService,
    private loginService: LoginService
  ) {
    this.getAllHappenings();
    this.display = true;
  }

  ngOnInit() {}

  edit(happening: Happening) {
    this.mode = 'edit';
    this.edited = happening;
    this.selected = happening;
    this.display = false;
  }

  createNew(happening) {
    this.mode = 'edit';
    this.selected = new Happening();
    this.display = true;
  }

  select(happening) {
    if (happening !== this.selected) {
      this.mode = 'selected';
    }
    this.selected = happening;
    // console.log(this.selected);
  }

  getAllHappenings() {
    this.eventService.getAllHappenings().subscribe((res: Happening[]) => {
      this.happenings = res;
      console.log(this.happenings);
    });
  }

  createHappening() {
    this.eventService
      .createHappening(this.selected, this.loginService.getCurrentUsername())
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Happening added sucessfully');
          this.getAllHappenings();
          this.mode = '';
        } else {
          alert('Happening could not be add');
          // tslint:disable-next-line:max-line-length
        }
      });
  }

  updateHappening() {
    console.log(this.selected);
    this.eventService
      .updateHappening(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Happening updated sucessfully');
          this.getAllHappenings();
          this.mode = '';
        } else {
          alert('Happening could not be updated');
          // tslint:disable-next-line:max-line-length
        }
      });
  }

  deleteHappening() {
    this.eventService
      .deleteHappening(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Happening added sucessfully');
          this.getAllHappenings();
          this.mode = '';
        } else {
          alert('Happening could not be add');
        }
      });
  }

  removeGMT(date1: Date) {
    const date = new Date(date1);
    // tslint:disable-next-line:no-construct
    // tslint:disable-next-line:quotemark
    // tslint:disable-next-line:no-construct
    let str = new String(date.toUTCString());
    str = str.slice(5, -7);
    return str;
  }

  convert(date: Date) {
    // tslint:disable-next-line:prefer-const
    // tslint:disable-next-line:no-construct
    const value = moment.utc(date).valueOf();
    return value;
  }
}
