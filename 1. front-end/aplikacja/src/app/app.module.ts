import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routerModule } from './app.routing';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { AllReservationsComponent } from './all-reservations/all-reservations.component';
import { AllRoomsComponent } from './all-rooms/all-rooms.component';
import { RoomService } from './service/room.service';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AllUsersComponent } from './all-users/all-users.component';
import { UserService } from './service/user.service';
import { ReservationService } from './service/reservation.service';
import { SubjectService } from './service/subject.service';
import {
  ScheduleModule,
  DialogModule,
  CalendarModule,
  ToggleButtonModule,
  DragDropModule,
  ButtonModule
} from 'primeng/primeng';
import {
  InputTextareaModule,
  InputTextModule,
  SelectButtonModule,
  DropdownModule,
  AutoCompleteModule
} from 'primeng/primeng';
import { CheckboxModule } from 'primeng/primeng';
import { AllEventsComponent } from './all-events/all-events.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './service/auth.service';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './auth.guard';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { AllSubjectsComponent } from './all-subjects/all-subjects.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableModule } from 'primeng/table';
import { SliderModule } from 'primeng/slider';
import { RegisterService } from './service/register.service';
import { LoginService } from './service/login.service';
import { ObservableRoomComponent } from './observable-room/observable-room.component';
import { ObservableRoomService } from './service/observableRoom.service';
import { EventService } from './service/events.service';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    AllReservationsComponent,
    AllRoomsComponent,
    AllUsersComponent,
    AllEventsComponent,
    LoginComponent,
    RegisterComponent,
    AllSubjectsComponent,
    ObservableRoomComponent
  ],
  imports: [
    BrowserModule,
    routerModule,
    CommonModule,
    FormsModule,
    HttpModule,
    ScheduleModule,
    AutoCompleteModule,
    DialogModule,
    CalendarModule,
    DropdownModule,
    ToggleButtonModule,
    DragDropModule,
    ButtonModule,
    InputTextareaModule,
    CheckboxModule,
    InputTextModule,
    SelectButtonModule,
    AngularDateTimePickerModule,
    BrowserModule,
    BrowserAnimationsModule,
    TableModule,
    SliderModule
  ],
  providers: [
    RoomService,
    UserService,
    ReservationService,
    SubjectService,
    AuthService,
    AuthGuard,
    RegisterService,
    LoginService,
    ObservableRoomService,
    EventService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
