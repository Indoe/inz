import { Component, OnInit } from '@angular/core';
import { Subject } from '../model/Subject';
import { SubjectService } from '../service/subject.service';
import { User } from '../model/User';
import { UserService } from '../service/user.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'all-subjects',
  templateUrl: './all-subjects.component.html',
  styleUrls: ['./all-subjects.component.css']
})
export class AllSubjectsComponent implements OnInit {
  subjects: Subject[];
  subject: Subject;
  selected: Subject = null;
  user: User;
  users: User[];
  mode = '';
  edited = {};

  edit(subject) {
    this.mode = 'edit';
    this.edited = subject;
    this.selected = subject;
  }

  createNew(subject) {
    this.mode = 'edit';
    this.selected = subject;
    this.edited = subject;
  }

  select(subject) {
    if (subject !== this.selected) {
      this.mode = 'selected';
    }
    this.selected = subject;
  }

  constructor(
    private subjectService: SubjectService,
    private userService: UserService
  ) {
    this.subject = new Subject();
    this.user = new User();

    this.subjectService.getAllSubjects().subscribe((res: Subject[]) => {
      this.subjects = res;
    });

    this.userService.getAllUsers().subscribe((res: User[]) => {
      this.users = res;
    });
  }

  getAllUsers() {
    this.subjectService.getAllSubjects().subscribe((res: Subject[]) => {
      this.subjects = res;
    });
  }

  getAllSubjects() {
    this.subjectService.getAllSubjects().subscribe((res: Subject[]) => {
      this.subjects = res;
    });
  }

  updateSubject() {
    this.subjectService
      .updateSubject(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Subject added sucessfully');
          this.getAllSubjects();
          this.mode = '';
        } else {
          alert('Subject could not be add');
        }
      });
  }

  deleteSubject() {
    console.log(this.subject);
    this.subjectService
      .deleteSubject(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Subject delted sucessfully');
          this.mode = '';
          this.getAllSubjects();
        } else {
          alert('Subject could not be delete');
        }
      });
  }

  ngOnInit() {}
}
