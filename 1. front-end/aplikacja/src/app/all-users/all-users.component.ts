import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { UserService } from '../service/user.service';
import { ActivatedRoute } from '@angular/router';
import { TableModule } from 'primeng/table';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit {
  users: User[];
  user: User;
  selected: User = null;
  edited = {};
  cols: any[];
  options: any[];
  displayDialog: boolean;

  edit(user) {
    this.displayDialog = true;
    this.edited = user;
    this.selected = user;
  }

  createNew(user) {
    this.displayDialog = true;
    this.edited = user;
    this.selected = user;
  }

  constructor(private userService: UserService, private route: ActivatedRoute) {
    this.getAllUsers();
    this.user = new User();
    this.cols = [
      { field: 'username', header: 'Username' },
      { field: 'name', header: 'Name' },
      { field: 'surname', header: 'Surname' },
      { field: 'email', header: 'Email' },
      { field: 'teacher', header: 'Teacher' },
      { field: 'admin', header: 'Admin' }
    ];

    this.options = [
      { label: 'All', value: null },
      { label: 'True', value: true },
      { label: 'False', value: false }
    ];
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe((res: User[]) => {
      this.users = res;
      console.log(this.users);
    });
  }

  updateUser() {
    this.userService.updateUser(this.selected).subscribe((res: Boolean) => {
      console.log(res);
      if (res) {
        alert('User added sucessfully');
        this.displayDialog = false;
        this.getAllUsers();
      } else {
        alert('User could not be add');
      }
    });
  }

  deleteUser() {
    console.log(this.user);
    this.userService.deleteUser(this.selected).subscribe((res: Boolean) => {
      console.log(res);
      if (res) {
        alert('User delted sucessfully');
        this.displayDialog = false;
        this.getAllUsers();
      } else {
        alert('User could not be delete');
      }
    });
  }

  ngOnInit() {}
}
