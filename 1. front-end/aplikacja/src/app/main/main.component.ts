import { Component, OnInit } from '@angular/core';
import { Reservation } from '../model/Reservation';
import { ReservationService } from '../service/reservation.service';
import { RoomService } from '../service/room.service';
import { Room } from '../model/Room';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  events: any[];
  reservations: Reservation[];
  room: Room;
  rooms: Room[];
  headerConfig: any;
  selectRoom: Room;
  start;
  end;

  ngOnInit() {}

  constructor(
    private roomService: RoomService,
    private reservationService: ReservationService
  ) {
    this.roomService.getAllRooms().subscribe((res: Room[]) => {
      this.rooms = res;
    });

    this.headerConfig = {
      left: 'prev,next today',
      center: 'title',
      right: 'nothing'
    };
  }

  loadEvents(event) {
    this.start = event.view.start._i;
    this.end = event.view.end._i;
    if (this.room) {
      this.getWeekReservations(this.start, this.end);
    }
  }

  getWeekReservations(start: String, end: String) {
    this.reservationService
      .getWeekResrvations(start, end, this.room)
      .subscribe((res: Reservation[]) => {
        this.reservations = res;
        this.transformReservationsToPrimeNgTable(this.reservations);
      });
  }

  transformReservationsToPrimeNgTable(reservations: Reservation[]) {
    const events2 = [];
    for (const reservation of this.reservations) {
      const temp = {
        title: reservation.description,
        start: reservation.startDate,
        end: reservation.endDate
      };
      events2.push(temp);
    }
    this.events = events2;
  }

  removeGMT(date1: Date) {
    const date = new Date(date1);
    // tslint:disable-next-line:no-construct
    // tslint:disable-next-line:quotemark
    // tslint:disable-next-line:no-construct
    let str = new String(date.toUTCString());
    str = str.slice(0, -3);
    return str;
  }

  roomChanged() {
    console.log('Room changed');
    this.getWeekReservations(this.start, this.end);
  }
}
