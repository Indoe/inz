import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AllReservationsComponent } from './all-reservations/all-reservations.component';
import { AllRoomsComponent } from './all-rooms/all-rooms.component';
import { AllUsersComponent } from './all-users/all-users.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { AllSubjectsComponent } from './all-subjects/all-subjects.component';
import { RegisterComponent } from './register/register.component';
import { ObservableRoomComponent } from './observable-room/observable-room.component';
import { AllEventsComponent } from './all-events/all-events.component';

const routesConfig: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: MainComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'reservations/all',
    component: AllReservationsComponent,
    canActivate: [AuthGuard]
  },
  { path: 'rooms/all', component: AllRoomsComponent, canActivate: [AuthGuard] },
  { path: 'users/all', component: AllUsersComponent, canActivate: [AuthGuard] },
  {
    path: 'subjects/all',
    component: AllSubjectsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'observableRoom/all',
    component: ObservableRoomComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'events/all',
    component: AllEventsComponent,
    canActivate: [AuthGuard]
  }
  /*{ path: '**', redirectTo: 'main', pathMatch: 'full' }*/
];

export const routerModule = RouterModule.forRoot(routesConfig, {});
