import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Subject } from '../model/Subject';

@Injectable()
export class SubjectService {
  private headers: Headers;
  private backendUrl: string;
  private options: RequestOptions;

  constructor(private http: Http) {
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.backendUrl = environment.backendUrl;
    this.options = new RequestOptions({ headers: this.headers });
    console.log('Initialize - Subject Service');
  }

  getAllSubjects(): Observable<Subject[]> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .get(this.backendUrl + '/subject/all/', options)
      .map(res => <Subject[]>res.json());
  }

  updateSubject(subject: Subject): Observable<Boolean> {
    console.log(subject);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/subject/update/', subject, options)
      .map(res => <Boolean>res.json());
  }

  deleteSubject(selected: Subject): Observable<Boolean> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/subject/delete/', selected, options)
      .map(res => <Boolean>res.json());
  }
}
