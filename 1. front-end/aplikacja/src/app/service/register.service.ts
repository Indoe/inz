import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { User } from '../model/User';

@Injectable()
export class RegisterService {
  private headers: Headers;
  private backendUrl: string;

  constructor(private http: Http, private router: Router) {
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.backendUrl = environment.backendUrl;
    console.log('Initialized - Register Service');
  }

  register(user: User) {
    const options = new RequestOptions({ headers: this.headers });
    const body = user;
    this.http
      .post(this.backendUrl + '/user/sign-up', body, options)
      .subscribe(res => {
        if (res.ok) {
          this.router.navigate(['/login']);
          alert('Registration successfull, you can log in');
        } else {
          alert('Registration unsuccessfull, please try again');
        }
      });
  }
}
