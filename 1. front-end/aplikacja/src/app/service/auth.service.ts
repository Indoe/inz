import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
  private headers: Headers;
  private backendUrl: string;
  private options: RequestOptions;

  private loggedIn = false;

  constructor(private http: Http) {
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.backendUrl = environment.backendUrl;
    this.options = new RequestOptions({ headers: this.headers });

    this.loggedIn = !!localStorage.getItem('auth_token');
  }

  logout() {
    localStorage.removeItem('currentUser');
  }
}
