import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import "rxjs/add/operator/map";
// tslint:disable-next-line:import-blacklist
import { Observable } from "rxjs/Rx";
import { environment } from "../../environments/environment";
import { Router } from "@angular/router";
import { ObservableRoom } from "../model/ObservableRoom";
import { LoginService } from "./login.service";

@Injectable()
export class ObservableRoomService {
  private headers: Headers;
  private backendUrl: string;
  private options: RequestOptions;

  constructor(private http: Http, private loginService: LoginService) {
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.backendUrl = environment.backendUrl;
    this.options = new RequestOptions({ headers: this.headers });
    console.log('Initialize - ObservableRoom Service');
  }

  getAllObservableRooms(): Observable<ObservableRoom[]> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .get(this.backendUrl + '/observableRoom/all/', options)
      .map(res => <ObservableRoom[]>res.json());
  }

  updateObservableRoom(observableRoom: ObservableRoom): Observable<Boolean> {
    console.log(observableRoom);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(
        this.backendUrl + '/observableRoom/update/',
        observableRoom,
        options
      )
      .map(res => <Boolean>res.json());
  }

  createObservableRoom(observableRoom: ObservableRoom): Observable<Boolean> {
    console.log(observableRoom);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(
        this.backendUrl +
          '/observableRoom/create/' +
          this.loginService.getCurrentUsername() +
          '/',
        observableRoom,
        options
      )
      .map(res => <Boolean>res.json());
  }

  deleteObservableRoom(selected: ObservableRoom): Observable<Boolean> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/observableRoom/delete/', selected, options)
      .map(res => <Boolean>res.json());
  }
}
