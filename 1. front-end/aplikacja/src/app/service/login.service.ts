import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { AuthenticationData } from '../../app/model/AuthenticationData';
import { UserService } from '../../app/service/user.service';
import { User } from '../model/User';
@Injectable()
export class LoginService {
  private headers: Headers;
  private backendUrl: string;
  private username: string;
  private token: string;
  private options: RequestOptions;

  constructor(private http: Http, private userService: UserService) {
    this.backendUrl = environment.backendUrl;
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: this.headers });
    console.log('Initialized - Register Service');
  }

  login(authenticationData: AuthenticationData): Observable<boolean> {
    const options = new RequestOptions({ headers: this.headers });
    const body = authenticationData;
    return this.http.post(this.backendUrl + '/login', body).map(res => {
      const token = res.headers.get('Authorization');
      if (token) {
        this.username = authenticationData.username;
        this.token = token;
        localStorage.setItem('currentUser', this.token);
        localStorage.setItem('username', this.username);
        this.userService.getLoggedUser(this.getCurrentUsername());
        return true;
      } else {
        return false;
      }
    });
  }

  logout() {
    localStorage.clear();
  }

  isUserLogged(): boolean {
    if (localStorage.getItem('currentUser')) {
      return true;
    }
    return false;
  }

  getCurrentUsername() {
    return localStorage.getItem('username');
  }
}
