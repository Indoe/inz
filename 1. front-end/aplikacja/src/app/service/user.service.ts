import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { User } from '../model/User';

@Injectable()
export class UserService {
  private headers: Headers;
  private backendUrl: string;
  private options: RequestOptions;

  userStream = new Subject();

  constructor(private http: Http) {
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.backendUrl = environment.backendUrl;
    this.options = new RequestOptions({ headers: this.headers });
    console.log('Initialize - User Service');
  }

  getUserSteram() {
    return this.userStream;
  }

  getAllUsers(): Observable<User[]> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .get(this.backendUrl + '/user/all/', options)
      .map(res => <User[]>res.json());
  }

  updateUser(user: User): Observable<Boolean> {
    console.log(user);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/user/update/', user, options)
      .map(res => <Boolean>res.json());
  }

  deleteUser(selected: User): Observable<Boolean> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/user/delete/', selected, options)
      .map(res => <Boolean>res.json());
  }

  getLoggedUser(username: String) {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .get(this.backendUrl + '/user/get/' + username + '/', options)
      .subscribe((res: Response) => {
        if (res.text() !== '') {
          const data = res.json();
          const user: User = data;
          return this.userStream.next(user);
        }
      });
  }
}
