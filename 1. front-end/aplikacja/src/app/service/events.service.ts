import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Reservation } from '../model/Reservation';
import { ScheduleModule } from 'primeng/primeng';
import { Room } from '../model/Room';
import { LoginService } from './login.service';
import { Happening } from '../model/Event';

@Injectable()
export class EventService {
  private headers: Headers;
  private backendUrl: string;
  private options: RequestOptions;

  constructor(private http: Http, private loginService: LoginService) {
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.backendUrl = environment.backendUrl;
    this.options = new RequestOptions({ headers: this.headers });
  }

  getAllHappenings(): Observable<Happening[]> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .get(this.backendUrl + '/event/all/', options)
      .map(res => <Happening[]>res.json());
  }

  createHappening(happening: Happening, username: String): Observable<Boolean> {
    console.log(happening);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(
        this.backendUrl + '/event/create/' + username + '/',
        happening,
        options
      )
      .map(res => <Boolean>res.json());
  }

  updateHappening(happening: Happening): Observable<Boolean> {
    console.log(happening);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/event/update/', happening, options)
      .map(res => <Boolean>res.json());
  }

  deleteHappening(selected: Happening): Observable<Boolean> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/event/delete/', selected, options)
      .map(res => <Boolean>res.json());
  }
}
