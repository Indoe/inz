import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Room } from '../model/Room';

@Injectable()
export class RoomService {
  private headers: Headers;
  private backendUrl: string;
  private options: RequestOptions;

  constructor(private http: Http) {
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.backendUrl = environment.backendUrl;
    this.options = new RequestOptions({ headers: this.headers });
    console.log('Initialize - Room Service');
  }

  getAllRooms(): Observable<Room[]> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .get(this.backendUrl + '/room/all/', options)
      .map(res => <Room[]>res.json());
  }

  updateRoom(room: Room): Observable<Boolean> {
    console.log(room);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/room/update/', room, options)
      .map(res => <Boolean>res.json());
  }

  deleteRoom(selected: Room): Observable<Boolean> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/room/delete/', selected, options)
      .map(res => <Boolean>res.json());
  }
}
