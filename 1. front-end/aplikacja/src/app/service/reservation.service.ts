import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Reservation } from '../model/Reservation';
import { ScheduleModule } from 'primeng/primeng';
import { Room } from '../model/Room';
import { LoginService } from './login.service';

@Injectable()
export class ReservationService {
  private headers: Headers;
  private backendUrl: string;
  private options: RequestOptions;

  constructor(private http: Http, private loginService: LoginService) {
    this.headers = new Headers({
      Authorization: localStorage.getItem('currentUser')
    });
    this.headers.append('Content-Type', 'application/json');
    this.backendUrl = environment.backendUrl;
    this.options = new RequestOptions({ headers: this.headers });
  }

  getAllReservations(): Observable<Reservation[]> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .get(this.backendUrl + '/reservation/all/', options)
      .map(res => <Reservation[]>res.json());
  }

  updateReservation(reservation: Reservation): Observable<Boolean> {
    console.log(reservation);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/reservation/update/', reservation, options)
      .map(res => <Boolean>res.json());
  }

  createReservation(
    reservation: Reservation,
    cycle: Number
  ): Observable<Boolean> {
    console.log(reservation);
    console.log(cycle);
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(
        this.backendUrl +
          '/reservation/create/' +
          cycle +
          '/' +
          this.loginService.getCurrentUsername() +
          '/',
        reservation,
        options
      )
      .map(res => <Boolean>res.json());
  }

  deleteReservation(selected: Reservation): Observable<Boolean> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(this.backendUrl + '/reservation/delete/', selected, options)
      .map(res => <Boolean>res.json());
  }

  getWeekResrvations(
    start: String,
    end: String,
    room: Room
  ): Observable<Reservation[]> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .get(
        this.backendUrl +
          '/reservation/getForWeek/' +
          start +
          '/' +
          end +
          '/' +
          room.id +
          '/',
        options
      )
      .map(res => <Reservation[]>res.json());
  }

  requestChangeReservation(selected: Reservation): Observable<Boolean> {
    const options = new RequestOptions({ headers: this.headers });
    return this.http
      .post(
        this.backendUrl +
          '/reservation/change/' +
          this.loginService.getCurrentUsername() +
          '/',
        selected,
        options
      )
      .map(res => <Boolean>res.json());
  }
}
