app.directive("bindTimestamp", function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {
      ngModel.$formatters.push(function(value) {
        console.log(value);
        return new Date(value);
      });
    }
  };
});
