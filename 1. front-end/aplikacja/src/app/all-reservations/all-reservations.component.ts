import { Component, OnInit } from '@angular/core';
import { Reservation } from '../model/Reservation';
import { ReservationService } from '../service/reservation.service';
import { ScheduleModule } from 'primeng/primeng';
import { Event } from '@angular/router/src/events';
import { Room } from '../model/Room';
import { User } from '../model/User';
import { CalendarModule } from 'primeng/primeng';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { UserService } from '../service/user.service';
import { Subject } from '../model/Subject';
import { SubjectService } from '../service/subject.service';
import { RoomService } from '../service/room.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'all-reservations',
  templateUrl: './all-reservations.component.html',
  styleUrls: ['./all-reservations.component.css']
})
export class AllReservationsComponent implements OnInit {
  settings = {
    bigBanner: true,
    timePicker: true,
    format: 'dd MMM yyyy hh:mm:ss.SSS',
    defaultOpen: false
  };
  cycle = 1;
  events: any[];
  reservations: Reservation[];
  reservation: Reservation;
  room: Room;
  rooms: Room[];
  user: User;
  users: User[];
  subject: Subject;
  subjects: Subject[];
  displayCycle: Boolean;
  selected: Reservation = null;
  mode = '';
  edited = {};
  admin = false;

  edit(reservation: Reservation) {
    this.displayCycle = false;
    this.mode = 'edit';
    this.edited = reservation;
    this.selected = reservation;
  }

  createNew(reservation) {
    this.mode = 'edit';
    this.displayCycle = true;
    this.selected = new Reservation();
  }

  select(reservation) {
    if (reservation !== this.selected) {
      this.mode = 'selected';
    }
    this.selected = reservation;
    // console.log(this.selected);
  }

  constructor(
    private reservationService: ReservationService,
    private userService: UserService,
    private roomService: RoomService,
    private subjectService: SubjectService
  ) {
    this.displayCycle = true;
    this.reservation = new Reservation();

    userService.getUserSteram().subscribe((res: User) => {
      this.admin = res.admin;
    });

    this.reservationService
      .getAllReservations()
      .subscribe((res: Reservation[]) => {
        this.reservations = res;
      });

    this.userService.getAllUsers().subscribe((res: User[]) => {
      this.users = res;
    });

    this.roomService.getAllRooms().subscribe((res: Room[]) => {
      this.rooms = res;
    });

    this.subjectService.getAllSubjects().subscribe((res: Subject[]) => {
      this.subjects = res;
    });
  }

  getAllReservations() {
    this.reservationService
      .getAllReservations()
      .subscribe((res: Reservation[]) => {
        this.reservations = res;
        console.log(this.reservations);
      });
  }

  requestReservation(selected: Reservation) {
    this.reservationService
      .requestChangeReservation(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Request sent successfully');
        } else {
          alert('Request unsuccessfully sent');
        }
      });
  }

  updateReservation() {
    console.log(this.selected);
    this.reservationService
      .updateReservation(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Reservation updated sucessfully');
          this.getAllReservations();
          this.mode = '';
        } else {
          alert('Reservation could not be updated');
          // tslint:disable-next-line:max-line-length
        }
      });
  }

  createReservation() {
    console.log(this.selected);
    this.reservationService
      .createReservation(this.selected, this.cycle)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Reservation added sucessfully');
          this.getAllReservations();
          this.mode = '';
        } else {
          alert('Reservation could not be add');
          // tslint:disable-next-line:max-line-length
        }
      });
  }

  deleteReservation() {
    this.reservationService
      .deleteReservation(this.selected)
      .subscribe((res: Boolean) => {
        console.log(res);
        if (res) {
          alert('Reservation added sucessfully');
          this.getAllReservations();
          this.mode = '';
        } else {
          alert('Reservation could not be add');
        }
      });
  }

  removeGMT(date1: Date) {
    const date = new Date(date1);
    // tslint:disable-next-line:no-construct
    // tslint:disable-next-line:quotemark
    // tslint:disable-next-line:no-construct
    let str = new String(date.toUTCString());
    str = str.slice(5, -7);
    return str;
  }

  convert(date: Date) {
    // tslint:disable-next-line:prefer-const
    // tslint:disable-next-line:no-construct
    const value = moment.utc(date).valueOf();
    return value;
  }

  ngOnInit() {}
}
